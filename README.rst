
Django Reactable
-----------

Django Reactable is an extension of Displayable. It makes it possible to leave reactions for a model by adding the Reactable base class to it. Possible reactions are:

- comments
- likes (with thumbs, and optionally 'liked by you, Henk, Ingrid and 7 others')
- dislikes (similar to likes; an optional vote distribution bar is available if likes and dislikes are both turned on)
- ratings (by default, client-side stars are provided by http://plugins.krajee.com/star-rating)

By adding a parent class to your model, it lets users react to that model by means of comments and (dis)likes. The model should be displayed with .display method, like ChainDisplayable.

Installation & Configuration
-----------

Make sure you have django-model-utils and Django Displayable, the later is available here: https://bitbucket.org/mverleg/django_displayable.git

- Install using ``pip install git+https://bitbucket.org/mverleg/django_reactable.git`` (or download and copy the app into your project).
- Add ``reactables`` to ``INSTALLED_APPS``.
- Add (something like) ``url(r'^react/', include(reactables.urls)),`` to your urls.
- To use the default styles, load ``{% static 'reactable.css' %}`` and ``{% static 'reactable.js' %}``

Quite some database changes are necessary

How to use
-----------

To make a model reactable, as a minimum change your model like this:

    from reactable.models import Reactable
    	class Example(Reactable):
    		pass

More realistically, add properties to control reactions:

    from reactable.models import Reactable
    class Example(Reactable):
    	allow_likes = True
    	allow_dislikes = False
    	allow_comments = True
    	allow_ratings = False
    	show_likes_bar = True # show a bar with likes/dislikes distribution
    	show_like_names = True # not if likes and dislikes are both on
    	likes_bar_scale = 10 # how many likes is many?

Then, to display reactions in a template for reactable instance ``example``, use 

    {% load react %}
    {% reactions example %}

Make sure you have the urls to post reactions, see Installation & Configuration

All the reactions are available in the django admin.

Future
-----------

- Especially the DisplayableReactable subclass needs more testing 
- Comments should themselves be reactable, most notably commentable
- Investigate if GenericForeignKey works better (advantage: much fewer database changes needed; disadvantage: complexity)
- Possibly make everything more customizable (own reactions, custom templates etc)
- Template tag to display briefly summarized reactions (nr likes, avg rating, nr comments)

License
-----------

django_reactable is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.


