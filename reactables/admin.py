
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from reactables.models import Comment
from reactables.models import Like, Dislike
from reactables.models import Rating


class ReactableAdmin(ModelAdmin):
	fields = ('user', 'target', 'posted',)
	list_display = ('user', 'target', 'posted',)
	readonly_fields = ('posted',)
	ordering = ('-posted',)


class CommentAdmin(ReactableAdmin):
	fields = ('user', 'target', 'message', 'posted', 'deleted',)
	list_display = ('user', 'target', 'preview', 'posted', 'is_deleted',)
	ordering = ('deleted', '-posted',)


class RatingAdmin(ReactableAdmin):
	fields = ('user', 'target', 'value', 'posted',)
	list_display = ('user', 'target', 'value', 'posted',)


admin.site.register(Like, ReactableAdmin)
admin.site.register(Dislike, ReactableAdmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Comment, CommentAdmin)


