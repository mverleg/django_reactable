
from django.core.urlresolvers import reverse
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from django import forms
from reactables.models import Comment
from reactables.forms.reaction import ReactionForm


'''
	form to place a new comment
'''
class CommentForm(ReactionForm):
	
	class Meta:
		model = Comment
		fields = ('message', 'target')
		widgets = {'target': forms.HiddenInput()}
	
	def __init__(self, data = None, back = '', *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_action = reverse('post_comment')
		if back:
			self.helper.add_input(Button('back', 'Back', onclick = 'location.href=\'%s\'' % back, css_class = 'btn-danger'))
		self.helper.add_input(Submit('comment', 'Post comment'))
		super(CommentForm, self).__init__(data, *args, **kwargs)


"""
class CommentDeleteForm(ReactionForm):
	
	class Meta:
		model = Comment
		fields = ('target',)
		widgets = {'target': forms.HiddenInput()}
	
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_action = reverse('delete_comment')
		self.helper.add_input(Submit('delete', 'delete comment'))
		super(CommentDeleteForm, self).__init__(*args, **kwargs)
"""

