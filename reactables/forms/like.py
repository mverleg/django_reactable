
from django.core.urlresolvers import reverse
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from django import forms
from reactables.models import Like, Dislike
from reactables.forms.reaction import ReactionForm


'''
	forms to like/dislike something
'''
class LikeForm(ReactionForm):
	
	class Meta:
		model = Like
		fields = ('target',)
		widgets = {'target': forms.HiddenInput()}
	
	def __init__(self, data = None, undo = False, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_action = reverse('post_like')
		if undo:
			self.helper.add_input(Submit('undo', 'undo like', css_class = 'btn-warning btn-xs'))
		else:
			self.helper.add_input(Submit('submit', 'like', css_class = 'btn-success btn-xs'))
		super(LikeForm, self).__init__(data, *args, **kwargs)


class DislikeForm(ReactionForm):
	
	class Meta:
		model = Dislike
		fields = ('target',)
		widgets = {'target': forms.HiddenInput()}
	
	def __init__(self, data = None, undo = False, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_action = reverse('post_dislike')
		if undo:
			self.helper.add_input(Submit('undo', 'undo dislike', css_class = 'btn-warning btn-xs'))
		else:
			self.helper.add_input(Submit('submit', 'dislike', css_class = 'btn-danger btn-xs'))
		super(DislikeForm, self).__init__(data, *args, **kwargs)


