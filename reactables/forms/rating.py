
from django.core.urlresolvers import reverse
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from django import forms
from reactables.models import Rating
from reactables.forms.reaction import ReactionForm
from reactables.models.rating import POSSIBLE_RATINGS


'''
	form to leave a rating
'''
class RatingForm(ReactionForm):
	
	class Meta:
		model = Rating
		fields = ('value', 'target')
		widgets = {'target': forms.HiddenInput(), 'value': forms.NumberInput()}
	
	def __init__(self, data = None, back = '', *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_action = reverse('post_rating')
		if back:
			self.helper.add_input(Button('back', 'Back', onclick = 'location.href=\'%s\'' % back, css_class = 'btn-danger'))
		self.helper.add_input(Submit('rate', 'Rate'))
		super(RatingForm, self).__init__(data, *args, **kwargs)
		self.fields['value'].widget.attrs['class'] = 'convert-to-star-rating'


