
from django import forms


'''
	base form for other specific reactions
'''
class ReactionForm(forms.ModelForm):
	
	next = forms.CharField(max_length = 128, widget = forms.HiddenInput)
	
	class Meta:
		''' doesn't inherit well; override in subclasses '''
		fields = ('target',)
		widgets = {'target': forms.HiddenInput()}
	
	def save(self, user, commit = True):
		comment = super(ReactionForm, self).save(commit = False)
		comment.user = user
		if commit:
			comment.save()
		return comment
	

