# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations
import displayable.classes
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Dislike',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('posted', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Like',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('posted', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('posted', models.DateTimeField(auto_now_add=True)),
                ('value', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reactable',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('reactable_ptr', models.OneToOneField(serialize=False, auto_created=True, primary_key=True, to='reactables.Reactable', parent_link=True)),
                ('posted', models.DateTimeField(auto_now_add=True)),
                ('message', models.TextField()),
                ('deleted', models.DateTimeField(blank=True, help_text='the time this comment was deleted (leave blank to not delete it)', null=True, default=None)),
                ('target', models.ForeignKey(to='reactables.Reactable', related_name='comments')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='comments')),
            ],
            options={
                'ordering': ('-posted',),
            },
            bases=('reactables.reactable', models.Model, displayable.classes.DirectDisplayable),
        ),
        migrations.AddField(
            model_name='rating',
            name='target',
            field=models.ForeignKey(to='reactables.Reactable', related_name='ratings'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rating',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='ratings'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='rating',
            unique_together=set([('user', 'target')]),
        ),
        migrations.AddField(
            model_name='like',
            name='target',
            field=models.ForeignKey(to='reactables.Reactable', related_name='likes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='like',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='likes'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='like',
            unique_together=set([('user', 'target')]),
        ),
        migrations.AddField(
            model_name='dislike',
            name='target',
            field=models.ForeignKey(to='reactables.Reactable', related_name='dislikes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dislike',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='dislikes'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='dislike',
            unique_together=set([('user', 'target')]),
        ),
    ]
