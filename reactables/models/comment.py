
'''
	a comment to leave on a Reactable
'''

from displayable import DirectDisplayable
from django.db import models
from django.template.loader import render_to_string
from reactables.models.reaction import Reaction
from reactables.models.reactable import Reactable


class Comment(Reaction, Reactable, DirectDisplayable):
	
	message = models.TextField()
	deleted = models.DateTimeField(null = True, blank = True, default = None, help_text = 'the time this comment was deleted (leave blank to not delete it)')
	
	class Meta:
		app_label = 'reactables'
		ordering = ('-posted',)
	
	def __unicode__(self):
		return 'comment by %s for %s: "%.30s"' % (self.user, self.target, self.message)
	
	def display(self, context, request, delete_button = True,**kwargs):
		return render_to_string('display_comment.html', {
			'comment': self,
			'delete_button': delete_button,
		}, context)
	
	def is_deleted(self):
		return not self.deleted is None
	is_deleted.boolean = True
	
	def preview(self, chars = 70):
		msg = self.message.strip()
		if len(msg) > chars:
			return '%s...' % msg[:chars-3]
		else:
			return msg

