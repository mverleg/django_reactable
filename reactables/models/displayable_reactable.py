

'''
	reactable that forms part of ChainDisplayable chain, automatically showing reactions
	and calling display_child to load the reacted content
'''

from displayable.classes import ChainDisplayable
from reactables.models.reactable import Reactable
from django.template.loader import render_to_string
from reactables.forms.comment import CommentForm


class ChainReactable(Reactable, ChainDisplayable):
	
	TEMPLATE = 'chain_reactable.html'
	
	class Meta:
		app_label = 'reactables'
		abstract = True
	
	def display(self, context, request, **kwargs):
		form = None
		if self.allow_comments:
			form = CommentForm(initial = {'next': request.get_full_path(), 'target': self})
		return render_to_string(self.TEMPLATE, {
			'displayable': self,
			'form': form,
		}, context)
		# create this form as a method on reactable


