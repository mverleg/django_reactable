
'''
	a like or dislike to leave on a Reactable
'''

from reactables.models.reaction import Reaction


class Like(Reaction):
	
	class Meta:
		unique_together = ('user', 'target',)
		app_label = 'reactables'
	
	def __unicode__(self):
		return 'like by %s for %s' % (self.user, self.target)


class Dislike(Reaction):
	
	class Meta:
		unique_together = ('user', 'target',)
		app_label = 'reactables'

	def __unicode__(self):
		return 'dislike by %s for %s' % (self.user, self.target)
