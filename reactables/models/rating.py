
'''
	a like or dislike to leave on a Reactable
'''

from django.db import models
from reactables.models.reaction import Reaction

POSSIBLE_RATINGS = ((1, 1), (2, 2), (3, 3), (4, 4), (5, 5),)


class Rating(Reaction):
	
	value = models.IntegerField(choices = POSSIBLE_RATINGS)
	
	class Meta:
		unique_together = ('user', 'target',)
		app_label = 'reactables'
	
	def __unicode__(self):
		return 'rating by %s for %s: %s (%d)' % (self.user, self.target, '*' * self.value, self.value)


