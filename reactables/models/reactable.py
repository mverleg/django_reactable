
'''
	a parent class that makes a model reactable
	it is not abstract because it is the target of foreign keys
'''
from math import log

from django.db import models
from model_utils.managers import InheritanceManager
from reactables.list_sample import list_sample_special


class AutoSubclassManager(InheritanceManager):
	def get_queryset(self):
		return super(AutoSubclassManager, self).get_queryset().select_subclasses()


class Reactable(models.Model):
	
	allow_likes = True
	allow_dislikes = False
	allow_comments = True
	allow_ratings = False
	
	show_likes_bar = True
	show_like_names = False # if likes XOR dislikes
	likes_bar_scale = 10 # how many likes is many?
	
	objects = AutoSubclassManager()
	
	class Meta:
		app_label = 'reactables'
	
	def __unicode__(self):
		return '%s #%s' % (self.__class__.__name__, self.pk)
	
	def total_likes(self):
		from reactables.models import Like
		if not self.allow_likes:
			return 0
		return Like.objects.filter(target = self).count()
	
	def total_dislikes(self):
		from reactables.models import Dislike
		if not self.allow_dislikes:
			return 0
		return Dislike.objects.filter(target = self).count()
	
	def total_likes_dislikes(self):
		return self.total_likes() + self.total_dislikes()
	
	def liked_percentage(self):
		likes, dislikes = self.total_likes(), self.total_dislikes()
		if likes or dislikes:
			return int(round(100. * likes / (likes + dislikes)))
		return None
	
	def disliked_percentage(self):
		return 100 - self.liked_percentage()
	
	def total_likes_scale(self):
		return 100 * min(log(1 + self.total_likes() + self.total_dislikes()) / log(self.likes_bar_scale), 2)
	
	def net_likes(self):
		return self.total_likes() - self.total_dislikes()
	
	def liked_by(self):
		if not self.allow_likes:
			return []
		return [like.user for like in self.likes.all()]
	
	def disliked_by(self):
		if not self.allow_dislikes:
			return []
		return [dislike.user for dislike in self.dislikes.all()]
	
	def liked_sample(self, user = None):
		return list_sample_special(self.liked_by(), special_item = user, limit = 3)
	
	def disliked_sample(self, user = None):
		return list_sample_special(self.disliked_by(), special_item = user, limit = 3)
	
	def total_comments(self):
		from reactables.models import Comment
		if not self.allow_comments:
			return 0
		return Comment.objects.filter(target = self).count()
	
	''' the number of rating! (for name consistency) '''
	def total_ratings(self):
		from reactables.models import Rating
		if not self.allow_ratings:
			return 0
		return Rating.objects.filter(target = self).count()
	
	def average_rating(self):
		from reactables.models import Rating
		if not self.allow_ratings:
			return 0
		return Rating.objects.filter(target = self).aggregate(models.Avg('value'))['value__avg'] or 0.


