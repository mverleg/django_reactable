
'''
	a basic reaction (to be subclassed by more specific ones)
'''

from django.db import models
from settings import AUTH_USER_MODEL
from reactables.models.reactable import Reactable


class Reaction(models.Model):
	
	target = models.ForeignKey(Reactable, related_name = '%(class)ss', db_index = True)
	user = models.ForeignKey(AUTH_USER_MODEL, related_name = '%(class)ss')
	posted = models.DateTimeField(auto_now_add = True, blank = True)
	
	class Meta:
		app_label = 'reactables'
		abstract = True


