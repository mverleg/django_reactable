
/*
	reactable scripts
*/

$(document).ready(function() {
	
	/*
		star ratings (jQuery, Bootstrap)
		from: http://plugins.krajee.com/star-rating
	*/
	$('.convert-to-star-rating').each(function(k)
	{
		/* get some elements (form; current status) */
		var form = $($(this).parent().parent().parent()[0]);
		var my_rating = $(form.parent());
		var current = $(my_rating.next()[0]);
		//var that = $(current.find('.star-rating-status'));
		var count = $(current.next()[0]);
		var locked = false;
		
		/* show the current status form */
		//$(that).rating({'min': 0, 'max': 5, 'step': 0.1, 'stars': 5, 'showClear': true, 'showCaption': false, 'size': 'xs', 'readonly': true});
		
		/* show my rating form and make interactive */
		form.find('label').hide();
		form.find('input[type=submit]').hide();
		$(this).rating({'min': 0, 'max': 5, 'step': 1, 'stars': 5, 'showClear': true, 'showCaption': true, 'size': 'xs', 'starCaptionClasses': {
		    1: 'label label-danger',
		    2: 'label label-danger',
		    3: 'label label-warning',
		    4: 'label label-success',
		    5: 'label label-success'
		}});
		submit_rating = function(form, event, value, caption) {
			form.submit();
			locked = true;
		}.bind(null, form);
		$(this).on('rating.change', submit_rating);
		$(this).on('rating.clear', submit_rating);
		
		/* make sure the correct form is displayed */
		my_rating.hide();
		current.mouseenter(function() {
			if ( ! locked) {
				current.hide();
				count.hide();
				my_rating.show();
			}
		});
		my_rating.mouseleave(function() {
			if ( ! locked) {
				current.show();
				count.show();
				my_rating.hide();
			}
		});
	});
	
	$('.star-rating-status').each(function(k)
	{
		/* show the current status form */
		$(this).rating({'min': 0, 'max': 5, 'step': 0.1, 'stars': 5, 'showClear': true, 'showCaption': false, 'size': 'xs', 'readonly': true});
	});
});


