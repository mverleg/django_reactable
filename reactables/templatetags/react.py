
'''
	template tags used by displayable subclasses
'''

from django.template.base import Library
from reactables.models import Reactable
from reactables.views.reactions import display_reactions


register = Library()

''' decorator to check for and pass request '''
def require_request(func):
	def with_request(context, *args, **kwargs):
		if 'request' in context:
			return func(context, context['request'], *args, **kwargs)
		raise Exception('request not found in context; make sure  that "django.core.context_processors.request" is in settings.TEMPLATE_CONTEXT_PROCESSORS and that you are using RequestContext when rendering')
	return with_request


'''
	display the reactions (comments, ratings, likes) for a reactable 
'''
@register.simple_tag(name = 'reactions', takes_context = True)
@require_request
def reactions(context, request, reactable, **kwargs):
	if not isinstance(reactable, Reactable):
		if reactable == '':
			raise Exception('{% reactions %} tag invoked with empty string argument; this likely means that the variable passed to {% reactions %} was not found; argument should be a Reactable subclass')
		raise Exception('{% reactions %} tag invoked with an unknown argument type %s; argument should be a Reactable subclass' % type(reactable))
	return display_reactions(request, reactable)


