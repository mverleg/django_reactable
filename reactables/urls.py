
from django.conf.urls import patterns, url
from views.comment import post_comment, delete_comment, delete_comment_confirm
from views.like import post_like, post_dislike
from views.rating import post_rating


urlpatterns = patterns('',
	url(r'^comment/$', post_comment, name = 'post_comment'),
	url(r'^comment/delete/confirm/$', delete_comment_confirm, name = 'delete_comment_confirm'),
	url(r'^comment/delete/$', delete_comment, name = 'delete_comment'),
	url(r'^like/$', post_like, name = 'post_like'),
	url(r'^dislike/$', post_dislike, name = 'post_dislike'),
	url(r'^rate/$', post_rating, name = 'post_rating'),
)


