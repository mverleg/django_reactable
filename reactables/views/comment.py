
'''
	save a submitted comment
'''

from displayable import DirectDisplayable, ChainDisplayable
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.decorators.http import require_POST
from reactables.forms.comment import CommentForm
from reactables.models import Comment
from django.core.urlresolvers import reverse
from reactables.views.notification import notification


@require_POST
@login_required
def post_comment(request):
	form = CommentForm(request.POST, back = request.POST['next'], initial = {'user': request.user})
	if form.is_valid():
		if not form.cleaned_data['target'].allow_comments:
			return notification(request, message = 'This item does not currently accept comments', subject = 'No comments allowed', next = form.cleaned_data['next'])
		form.save(request.user)
		return redirect(to = form.cleaned_data['next'])
	target = form.cleaned_data['target']
	if not isinstance(target, DirectDisplayable) and not isinstance(target, ChainDisplayable):
		target = None
	url = None
	if hasattr(target, 'get_absolute_url'):
		url = target.get_absolute_url()
	return render(request, 'fix_form.html', {
		'form': form,
		'target': target,
		'url': url,
	})


@require_POST
@login_required
def delete_comment_confirm(request):
	try:
		comment = Comment.objects.get(pk = int(request.POST['comment_pk']))
	except:
		return notification(request, 
			message = 'The request to delete the comment was invalid; it has not been deleted.', 
			subject = 'Invalid', 
			next = request.POST['next'] if 'next' in request.POST else None
		)
	return render(request, 'confirm_first.html', {
		'post': [(key, val) for key, val in request.POST.items() if not key.startswith('csrf')],
		'subject': 'Delete comment?',
		'message': '<p>Are you sure you want to delete this comment?</p>%s' % comment.display(context = None, request = request, delete_button = False),
		'cancel_url': request.POST['next'] if 'next' in request.POST else '/',
		'submit_url': reverse('delete_comment'),
		'submit_text': 'delete',
		'submit_class': 'btn-danger',
	})


@require_POST
@login_required
def delete_comment(request):
	try:
		comment_pk, next = [request.POST[key] for key in ('comment_pk', 'next',)]
		comment = Comment.objects.get(pk = int(comment_pk))
	except:
		return notification(request, 
			message = 'The request to delete the comment was invalid; it has not been deleted.', 
			subject = 'Invalid', 
			next = request.POST['next'] if 'next' in request.POST else None
		)
	if comment.user.pk == request.user.pk or request.user.is_staff:
		comment.deleted = timezone.now()
		comment.save()
		return redirect(to = next)
	return notification(request, 
		message = 'That is not your comment; you can not delete it.', 
		subject = 'Invalid', 
		next = request.POST['next'] if 'next' in request.POST else None
	)


