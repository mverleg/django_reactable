
'''
	save a submitted like, or remove it if it already exists, and always 
	remove the dislikes if there is a change in likes
	(exact opposite for dislikes, of course)
'''

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.views.decorators.http import require_POST
from reactables.forms.like import LikeForm, DislikeForm
from reactables.models import Like, Dislike
from reactables.views.notification import notification


'''
	generalized like-dislike view to prevent code duplication	
'''
def post_like_dislike(request, Add, AddForm, Remove):
	form = AddForm(data = request.POST, undo = ('undo' in request.POST), initial = {'user': request.user})
	if form.is_valid():
		if not form.cleaned_data['target'].allow_likes and not form.cleaned_data['target'].allow_dislikes:
			return notification(request, message = 'This item does not currently accept likes or dislikes', subject = 'No likes or dislikes allowed', next = form.cleaned_data['next'])
		target, next = form.cleaned_data['target'], form.cleaned_data['next']
		(add, remove,) = (Model.objects.filter(user = request.user, target = target) for Model in (Add, Remove,))
		if remove:
			remove.delete()
		if add:
			add.delete()
		else:
			Add(target = target, user = request.user).save()
		return redirect(to = next)
	return notification(request, 
		message = 'The request to place a %s was invalid; it has not been placed.' % Add.__name__.lower(), 
		subject = 'Invalid', 
		next = request.POST['next'] if 'next' in request.POST else None
	)


@require_POST
@login_required
def post_like(request):
	return post_like_dislike(request, Like, LikeForm, Dislike)

@require_POST
@login_required
def post_dislike(request):
	return post_like_dislike(request, Dislike, DislikeForm, Like)


