
'''
	save a submitted comment
'''

from displayable import DirectDisplayable, ChainDisplayable
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.views.decorators.http import require_POST
from reactables.forms.rating import RatingForm
from reactables.models import Rating
from reactables.views.notification import notification


@require_POST
@login_required
def post_rating(request):
	validified_POST = dict((key, '1') if key == 'value' and val.strip() == '0' else (key, val) for key, val in request.POST.items())
	form = RatingForm(validified_POST, back = request.POST['next'], initial = {'user': request.user})
	if form.is_valid():
		if not form.cleaned_data['target'].allow_ratings:
			return notification(request, message = 'This item does not currently accept ratings', subject = 'No ratings allowed', next = form.cleaned_data['next'])
		target = form.cleaned_data['target']
		ratings = Rating.objects.filter(user = request.user, target = target)
		if ratings:
			ratings.delete()
		if int(request.POST['value']) > 0:
			form.save(request.user)
		return redirect(to = form.cleaned_data['next'])
	target = form.cleaned_data['target']
	if not isinstance(target, DirectDisplayable) and not isinstance(target, ChainDisplayable):
		target = None
	url = None
	if hasattr(target, 'get_absolute_url'):
		url = target.get_absolute_url()
	return render(request, 'fix_form.html', {
		'form': form,
		'target': target,
		'url': url,
	})


