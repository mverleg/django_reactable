
'''
	display reactions for a reactable; to be called from {% reactions %} tag
	not quite a view as it returns a string, and shouldn't be attacked to a url
'''

from django.template import RequestContext
from django.template.loader import render_to_string
from reactables.models import Comment, Like, Dislike, Rating
from reactables.forms.comment import CommentForm#, CommentDeleteForm
from reactables.forms.like import LikeForm, DislikeForm
from reactables.forms.rating import RatingForm


def display_reactions(request, reactable):
	context_dict = {
		'reactable': reactable,
		'user_likes': None,
		'user_dislikes': None, 
		'user_ratings': None,
		'liked_sample': reactable.liked_sample(request.user),
		'disliked_sample': reactable.disliked_sample(request.user),
	}
	if request.user.is_authenticated():
		(user_comments, user_likes, user_dislikes, user_ratings,) = [Model.objects.filter(target = reactable, user = request.user) for Model in (Comment, Like, Dislike, Rating,)]
		context_dict.update({
			'user_likes': user_likes,
			'user_dislikes': user_dislikes, 
			'user_ratings': user_ratings,
			'comment_form': CommentForm(initial = {'target': reactable, 'next': request.get_full_path(), }),
			'like_form': LikeForm(undo = bool(user_likes), initial = {'target': reactable, 'next': request.get_full_path(), }),
			'dislike_form': DislikeForm(undo = bool(user_dislikes), initial = {'target': reactable, 'next': request.get_full_path(), }),
			'rating_form': RatingForm(initial = {'target': reactable, 'next': request.get_full_path(), 'value': user_ratings[0].value if user_ratings else None}),
		})
	return render_to_string('reactions.html', context_dict, RequestContext(request))


