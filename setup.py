# -*- coding: utf-8 -*-

"""
    for installing with pip
"""

from distutils.core import setup
from setuptools import find_packages

setup(
    name='reactable',
    version='1.1.0',
    author='Mark V',
    author_email='noreply.mail.nl',
    packages=find_packages(),
    include_package_data=True,
    url='git+https://bitbucket.org/mverleg/django_reactable.git',
    license='revised BSD license; see LICENSE.txt',
    description='see readme',
    zip_safe=True,
    install_requires = ['django_displayable', 'django-model-utils',],
)
